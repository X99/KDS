/**
 * @file keyStore.cpp
 * @author X99 (contact@x99.fr)
 * @brief An HTTP RSA Key store that handles key rotation and more.
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright None yet :)
 */

#include "keyStore.h"


KeyStore::KeyStore(std::chrono::minutes key_lease_duration, uint16_t port)
    : _port{port}, _key_lease_duration{key_lease_duration} {
    _srv = new httplib::SSLServer{"./cert/cert.pem", "./cert/key.pem"};

    if (_srv->is_valid()) {
        _srv->Get("/status", [this](const httplib::Request& req, httplib::Response& res) {
            this->status(req, res);
        });

        _srv->Get("/pub_keys", [this](const httplib::Request& req, httplib::Response& res) {
            this->send_public_keys(req, res);
        });

        _srv->Get("/priv_key", [this](const httplib::Request& req, httplib::Response& res) {
            this->send_private_key(req, res);
        });
    }
}

KeyStore::~KeyStore() {
    _srv->stop();

    while (_srv->is_running()) {}

    delete _srv;
}

// TODO(X99): remove this
std::string dump_headers(const httplib::Headers &headers) {
    std::string s;
    char buf[BUFSIZ];

    for (auto it = headers.begin(); it != headers.end(); ++it) {
        const auto &x = *it;
        snprintf(buf, sizeof(buf), "%s: %s\n", x.first.c_str(), x.second.c_str());
        s += buf;
    }

    return s;
}

// TODO(X99): write real code for this one, with stats such as processed rqs/s
void KeyStore::status(const httplib::Request& req, httplib::Response& res) {
    (void)(req);
    res.set_content(dump_headers(req.headers), "text/plain");
}

/**
 * @brief Request responder for /pub_keys path. Sends a JSON array containing all the public keys and their rotation date
 *
 * @param req
 * @param res
 */
void KeyStore::send_public_keys([[maybe_unused]] const httplib::Request& req, httplib::Response& res) {
    nlohmann::json json;
    for (const auto& [key, value] : key_store) {
        json.push_back(value.to_json(_key_lease_duration));
    }

    res.set_content(json.empty()? "{}" : json.dump(), "application/json");
}

void KeyStore::send_private_key(const httplib::Request& req, httplib::Response& res) {
    nlohmann::json json = {};
    if (req.has_header("mrsigner")) {
        // get MrSigner value from header
        std::string value = req.get_header_value("mrsigner");

        // check and convert it
        MrSigner m = decode_mrsigner(value);

        // is this mrsigner in our whitelist?
        if (m.size() == 32 && is_mrsigner_in_whitelist(m)) {
            // yes it is, get the IP from the header and save it
            std::string ip_from_header = req.get_header_value("REMOTE_ADDR");
            // retrieve the corresponding private key
            KeySet& key_set = key_store[m];
            // set the IP from the caller
            key_set.ip.from_string(ip_from_header);
            // retrieve the private key
            json = key_set.current_private_key_to_json();
        }
    }

    res.set_content(json.empty()? "{}" : json.dump(), "application/json");
}

/**
 * @brief Starts the HTTP server and associated key rotation thread
 *
 * @return true
 * @return false
 */
bool KeyStore::start() {
    // set off main thread
    _server_thread = new std::thread([&]() { _srv->listen("0.0.0.0", _port); });
    _server_thread->detach();

    for (uint8_t retries = 0; retries <= 10; retries++) {
        if (_srv->is_running())
            break;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    // set off key regeneration thread
    _key_rotation_thread = new std::thread(&KeyStore::rotate_keys, this);
    _key_rotation_thread->detach();

    return _srv->is_running();
}

/**
 * @brief Stops the HTTP server and associated key rotation thread
 *
 * @return true
 * @return false
 */
bool KeyStore::stop() {
    _srv->stop();

    for (uint8_t retries = 0; retries <= 10; retries++) {
        if (!_srv->is_running())
            break;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    delete _server_thread;
    delete _key_rotation_thread;

    return _srv->is_running();
}

/**
 * @brief Add an array of MrSigners to the whitelist
 *
 * @param mr_signer_list
 */
void KeyStore::whitelist(const std::initializer_list<MrSigner>& mr_signer_list) {
    for (MrSigner mr_signer : mr_signer_list) {
        mr_signer_whitelist.push_back(mr_signer);
    }
}

/**
 * @brief Adds a unique MrSigner to the whitelist.
 *
 * @param mr_signer
 */
void KeyStore::whitelist(MrSigner& mr_signer) {
    mr_signer_whitelist.push_back(mr_signer);
}

/**
 * @brief Goes through the whitelist and generates an RSA key pair for each of them.
 *
 * @return true
 * @return false
 */
bool KeyStore::generate_all_keys() {
    bool all_is_ok = true;
    for (MrSigner mr_signer : mr_signer_whitelist) {
        all_is_ok |= generate_key(mr_signer);
    }

    return all_is_ok;
}

/**
 * @brief Key rotation thread function.
 * This function goes through all KeySets and updates outdated ones, according to the key_lease_duration parameter.
 */
void KeyStore::rotate_keys() {
    // as we may want, in the future, to add a new MrSigner while the server is running, this
    // code states that all KeySet.generation_time are not aligned.
    uint64_t counter{0};
    while (true) {
        std::this_thread::sleep_for(1s);

        for (auto& [key, value] : key_store) {
            auto now = std::chrono::system_clock::now();

            // fmt::print("no = {:%H:%M:%S}, rotation at {:%H:%M:%S}\n",
            //     fmt::localtime(now),
            //     fmt::localtime(value.generation_time + _key_lease_duration)
            // );

            // is it time to rotate the keys?
            if (now > value.generation_time + _key_lease_duration) {
                // fmt::print("Found a key to renew, renewing it\n");

                // ok, let's rotate the yes
                // 1 - safely remove the previous one
                RSA_free(value.previous_key);

                // TODO: use move semantics
                // 2 - swap keys
                value.previous_key = value.current_key;

                // 3 - overwrite current key with a new one
                value.current_key = Crypto::generate_new_key_pair();

                // 4 - set new generation time
                value.generation_time = std::chrono::system_clock::now();
            }
        }
        counter++;
    }
}

/**
 * @brief Generates an RSA keypair for a given MrSigner
 *
 * @param mr_signer
 * @return true
 * @return false
 */
bool KeyStore::generate_key(const MrSigner& mr_signer) {
    // better check if this MrSigner is whitelisted or not
    if (is_mrsigner_in_whitelist(mr_signer) == false)
        return false;

    // ok it's in the whitelist, let's generate a key pair for it...
    RSA* key_pair = Crypto::generate_new_key_pair();
    if (key_pair == nullptr)
        return false;

    // ...and store it
    KeySet key_set = {
        .previous_key = nullptr,
        .current_key = key_pair,
        .generation_time = std::chrono::system_clock::now()
    };
    key_store.insert(std::pair<MrSigner, KeySet>(mr_signer, key_set));

    return true;
}

/**
 * @brief Checks whether a given MrSigner is in our whitelist or not
 *
 * @param mr_signer
 * @return true
 * @return false
 */
bool KeyStore::is_mrsigner_in_whitelist(const MrSigner& mr_signer) {
    return (std::find(mr_signer_whitelist.begin(), mr_signer_whitelist.end(), mr_signer) != mr_signer_whitelist.end());
}

// TODO: implement this
// bool KeyStore::is_mr_signer_valid(const MrSigner& mr_signer) {
//     return true;
// }

MrSigner KeyStore::decode_mrsigner(const std::string & source) {
    if (std::string::npos != source.find_first_not_of("0123456789ABCDEFabcdef") || source.length() != 64) {
        // you can throw exception here
        return {};
    }

    union {
        uint64_t binary;
        char byte[8];
    } value{};

    auto size = source.size(), offset = (size % 16);
    MrSigner binary{};
    binary.reserve((size + 1) / 2);

    if (offset) {
        value.binary = std::stoull(source.substr(0, offset), nullptr, 16);

        for (auto index = (offset + 1) / 2; index--;) {
            binary.emplace_back(value.byte[index]);
        }
    }

    for ( ; offset < size; offset += 16 ) {
        value.binary = std::stoull(source.substr(offset, 16), nullptr, 16);
        for ( auto index = 8; index--; ) {
            binary.emplace_back(value.byte[index]);
        }
    }

    return binary;
}