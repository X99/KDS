/**
 * @file main.cpp
 * @author X99 (contact@x99.fr)
 * @brief None
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright None yet :)
 */

#include "main.h"

#include <fmt/core.h>
#include <fmt/format.h>

#include <csignal>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>

#include <nlohmann/json.hpp>

#include "keyStore.h"

KeyStore* k{nullptr};
volatile bool quit{false};

void sigint_handler(int signal) {
    if (signal == SIGINT) {
        fmt::print("\rIntercepted SIGINT, exiting...");
        k->stop();
        if (k != nullptr) {
            delete k;
        }
        quit = true;
    }
}

bool open_parameters(nlohmann::json& parameters) {
    std::ifstream file("config.json");
    if (file.fail()) {
        return false;
    }

    // unserialize it
    file >> parameters;

    return true;
}

MrSigner string_to_bytes(const std::string& str) {
    MrSigner bytes;

    for (uint32_t i = 0; i < str.length(); i += 2) {
        std::string byteString = str.substr(i, 2);
        unsigned char byte = static_cast<unsigned char>(strtol(byteString.c_str(), NULL, 16));

        // fmt::print("Adding {:#04x}\n", byte);

        bytes.push_back(byte);
    }

    return bytes;
}

using namespace std::literals::chrono_literals;

int main() {
    // install SIGINT handler
    std::signal(SIGINT, sigint_handler);

    // Init the Key Store
    fmt::print("Creating the KDS server...");
    if ((k = new KeyStore(2min, 8080)) == nullptr) {
        fmt::print("Could not instantiate Key Store, exiting...");
        return 1;
    }
    fmt::print("✅\n");


    // parse parameters
    nlohmann::json parameters;
    fmt::print("Parsing parameters...");
    if (open_parameters(parameters) == false) {
        fmt::print("Could not parse parameters, exiting...\n");
        return 1;
    }
    fmt::print("✅\n");

    fmt::print("Creating whitelist...");
    // retrieve MrSigner list as a vector
    std::vector<std::string> mr_signer_whitelist = parameters["mr_signer_whitelist"];
    for (std::string mr_signer : mr_signer_whitelist) {
        // veryfy MrSigner's format, should be 64 chars long (32 bytes once converted from ASCII)
        if (mr_signer.length() != 64) {
            fmt::print("Bad MrSigner (size != 64 characters): {}", mr_signer);
            continue;
        }

        // convert hex ASCII string to hex vector
        MrSigner temp_mr_signer = string_to_bytes(mr_signer);
        k->whitelist(temp_mr_signer);
    }

    size_t size = k->get_whitelist_size();
    fmt::print(" ({} MrSigners entries) ", size);
    if (size == 0) {
        fmt::print("WARNING: no instance will be able to connect to the server!");
    }
    fmt::print("✅\n");

    fmt::print("Generating key pairs...");
    if (k->generate_all_keys() == false) {
        fmt::print("error while generating key pairs, exiting.\n");
        return 1;
    }
    fmt::print("✅\n");

    // Start the key store server
    fmt::print("Starting the Key Store server...");
    if (k->start() == false) {
        fmt::print("error while starting the Key Store, exiting.\n");
        return 1;
    }
    fmt::print("✅\n");

    while (!quit) {}

    return 0;
}