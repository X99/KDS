/**
 * @file crypto.cpp
 * @author X99 (contact@x99.fr)
 * @brief An RSA key helping collection of tools
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright None yet :)
 */

#include "crypto.h"

RSA* Crypto::generate_new_key_pair(size_t size) {
    RSA* key_pair;
    BIGNUM* bne;

    int ret;
    int exponent = RSA_3;

    bne = BN_new();
    ret = BN_set_word(bne, exponent);
    key_pair = RSA_new();

    ret = RSA_generate_key_ex(key_pair, size, bne, nullptr);

    if (ret != 1) {
        return nullptr;
    }

    return key_pair;
}

void Crypto::print_rsa_key_pair(RSA* key_pair) {
    RSA_print_fp(stdout, key_pair, 0);
}

bool Crypto::save_keys_to_files(RSA* key_pair, std::string private_key_file, std::string public_key_file) {
    bool all_is_ok = false;
    BIO* bp_public = nullptr;
    BIO* bp_private = nullptr;

    if (public_key_file.empty() == false) {
        bp_public = BIO_new_file(public_key_file.c_str(), "w+");
        all_is_ok |= PEM_write_bio_RSAPublicKey(bp_public, key_pair);
        BIO_free_all(bp_public);
    }

    if (private_key_file.empty() == false) {
        bp_private = BIO_new_file(private_key_file.c_str(), "w+");
        all_is_ok |= PEM_write_bio_RSAPrivateKey(bp_private, key_pair, nullptr, nullptr, 0, nullptr, nullptr);
        BIO_free_all(bp_private);
    }

    return all_is_ok;
}

RSA* Crypto::get_public_key(RSA* key) {
    return RSAPublicKey_dup(key);
}
RSA* Crypto::get_private_key(RSA* key){
    return RSAPrivateKey_dup(key);
}

void Crypto::get_private_key(RSA* key_pair, std::string& out) {
    // Private key length
    size_t pri_len = 0;
    // private key
    char *pri_key = nullptr;

    // create IO buffer to store our private key to
    BIO *pri = BIO_new(BIO_s_mem());

    // extract the private key
    PEM_write_bio_RSAPrivateKey(pri, key_pair, NULL, NULL, 0, NULL, NULL);

    // get the key length and allocate room for it
    pri_len = BIO_pending(pri);
    pri_key = reinterpret_cast<char *>(malloc(pri_len + 1));

    // read the private key from the buffer
    BIO_read(pri, pri_key, pri_len);
    pri_key[pri_len] = '\0';

    // transforms the buffer to a std::string
    out = std::string(pri_key);

    // free allocated memory
    BIO_free_all(pri);
    free(pri_key);
}

void Crypto::get_public_key(RSA* key_pair, std::string& out) {
    // public key length
    size_t pub_len = 0;
    // public key
    char *pub_key = nullptr;

    // create IO buffer to store our public key to
    BIO *pub = BIO_new(BIO_s_mem());

    // extract the public key
    PEM_write_bio_RSA_PUBKEY(pub, key_pair);

    // get the key length and allocate room for it
    pub_len = BIO_pending(pub);
    pub_key = reinterpret_cast<char *>(malloc(pub_len + 1));

    // read the private key from the buffer
    BIO_read(pub, pub_key, pub_len);
    pub_key[pub_len] = '\0';

    out = std::string(pub_key);

    // free allocated memory
    BIO_free_all(pub);
    free(pub_key);
}

