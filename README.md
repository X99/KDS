# Installing dependencies

Dependencies installation is handled by the makefile. Just use

```
make setup
```

to:
- install dependencies
- init git submodules
- generate SSL key/cert.

KDS relies on these modules:
- [HTTPLib](https://github.com/yhirose/cpp-httplib) [v0.9.2](https://github.com/yhirose/cpp-httplib/releases/tag/v0.9.2)
- [JSON](https://github.com/nlohmann/json) [v3.9.1](https://github.com/nlohmann/json/releases/tag/v3.9.1)

# Docker

```
docker build -t kds .
docker run -d --name kds -p 8080:8080 kds
```