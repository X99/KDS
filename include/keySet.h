#pragma once

#include <chrono>
#include <string>
#include <nlohmann/json.hpp>

#include "./crypto.h"
#include "ipv4address.h"

/**
 * @brief A keyset is a pair of RSA keys: one that's in use, one
 * that's been used in the past and is keps for a certain delay
 * until it expires.
 * The new key has a generation time so we can keep track of the
 * renewal time.
 */
struct KeySet {
    // When using OpenSSL >= 1.1.0, you MUST use pointers. See here: https://wiki.openssl.org/index.php/OpenSSL_1.1.0_Changes

    /**
     * @brief The previous key overlaps the current one so the request coming back from the final
     * server can be decoded as well
     */
    RSA* previous_key;
    /**
     * @brief This is the RSA key used to encrypt transactions.
     * The private part of it is used by the proxy instances, its public counterpart
     * by the client.
     */
    RSA* current_key;
    /**
     * @brief This is the current key generation time. The KeyStore uses it to check
     * if the current key has to be renewed.
     */
    std::chrono::time_point<std::chrono::system_clock> generation_time;

    /**
     * @brief at a time during the execution, an instance of the Proxy will request a
     * private key from the Key Store. At that time, its IP can be extracted from the
     * HTTP request header and stored so we can pass it to the client along with public keys.
     */
    IPV4Address ip;

    nlohmann::json current_private_key_to_json() const {
        std::string private_key;
        Crypto::get_private_key(current_key, private_key);

        return nlohmann::json({"private_key:", private_key});
    }

    nlohmann::json to_json(std::chrono::minutes key_lease_duration) const {
        std::string public_key;
        Crypto::get_public_key(current_key, public_key);

        std::string old_public_key;
        Crypto::get_public_key(previous_key, old_public_key);

        std::string rotation_time = fmt::format(
            "{:%Y-%m-%d %H:%M:%S}",
            generation_time + key_lease_duration);

        return nlohmann::json({
            {"public_key", public_key},
            {"previous_public_key", old_public_key},
            {"rotation_date", rotation_time},
            {"associated_ip", ip.to_string()}
        });
    }
};
