/**
 * @file crypto.h
 * @author X99 (contact@x99.fr)
 * @brief An RSA key helping collection of tools
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright None yet :)
 */

#pragma once

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>

#include <nlohmann/json.hpp>

#include <memory>
#include <string>

namespace Crypto {
    // https://www.openssl.org/docs/man1.1.1/man3/RSA_generate_key.html
    RSA* generate_new_key_pair(size_t size = 2048);

    RSA* get_public_key(RSA*);
    RSA* get_private_key(RSA*);

    void get_public_key(RSA*, std::string&);
    void get_private_key(RSA*, std::string&);

    bool save_keys_to_files(RSA* key_pair, std::string private_key_file, std::string public_key_file);
    void print_rsa_key_pair(RSA* key_pair);
};
