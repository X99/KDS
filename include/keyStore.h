/**
 * @file keyStore.h
 * @author X99 (contact@x99.fr)
 * @brief An HTTP RSA Key store that handles key rotation and more.
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright None yet :)
 */

#pragma once

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include <httplib.h>

#ifndef NDEBUG
    #include <fmt/core.h>
    #include <fmt/chrono.h>
#endif

#include <map>
#include <string>
#include <vector>
#include <thread>
#include <utility>
#include <chrono>
#include <initializer_list>
#include <array>
#include <regex>
#include <algorithm>

#include "./crypto.h"
#include "keySet.h"


using std::literals::chrono_literals::operator""min;
using std::literals::chrono_literals::operator""s;
using MrSigner = std::vector<uint8_t>;

class KeyStore {
 private:
    httplib::SSLServer* _srv;

    std::thread* _server_thread;
    std::thread* _key_rotation_thread;

    uint16_t _port{0};
    std::chrono::minutes _key_lease_duration;

    std::map<MrSigner, KeySet> key_store;
    std::vector<MrSigner> mr_signer_whitelist;

    // responder functions
    void status(const httplib::Request&, httplib::Response&);
    void send_public_keys(const httplib::Request&, httplib::Response&);
    void send_private_key(const httplib::Request&, httplib::Response&);

    // internal key management
    void rotate_keys();
    bool generate_key(const MrSigner&);
    bool is_mrsigner_in_whitelist(const MrSigner&);
    MrSigner decode_mrsigner(const std::string&);

 public:
    explicit KeyStore(std::chrono::minutes key_lease_duration = 2min, uint16_t port = 8080);
    ~KeyStore();

    void whitelist(const std::initializer_list<MrSigner>&);
    void whitelist(MrSigner&);

    size_t get_whitelist_size() { return mr_signer_whitelist.size(); }

    bool start();
    bool stop();

    // key maintenance
    bool generate_all_keys();
};
