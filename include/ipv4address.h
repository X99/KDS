#pragma once

#include <string>

struct IPV4Address {
    using IP = std::array<int, 4>;
    IP ip{0};

    void from_string(const std::string& s) {
        std::regex re{R"((\d+))"};

        std::transform(
            std::sregex_token_iterator(s.begin(), s.end(), re),
            {},
            ip.begin(),
            [](const std::string& s){ return std::stoi(s); });
    }

    // Function to validate an IP address
    bool validateIP(const std::string& ip) const {
        std::regex regex{"^((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)(\\.(?!$)|$)){4}$"};
        return std::regex_match(ip, regex);
    }

    // converts to a string
    std::string to_string() const {
        std::string formatted_ip{};
        std::stringstream ss;
        ss << ip[0] << "." << ip[1] << "." << ip[2] << "." << ip[3];

        return ss.str();
    }
};