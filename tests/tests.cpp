/**
 * @file tests.cpp
 * @author X99 (contact@x99.fr)
 * @brief None
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright None yet :)
 */

#include <gtest/gtest.h>

#include "crypto.h"

#define GTEST_COUT std::cerr << "[          ] [ INFO ]"

TEST(CryptoTests, TestKeyGeneration) {
    RSA* key_pair = Crypto::generate_new_key_pair(2048);

    // verify return value as a whole
    EXPECT_NE(key_pair, nullptr);

    // verify common modulus
    EXPECT_NE(RSA_get0_n(key_pair), nullptr);

    // verify public/private exponents
    EXPECT_NE(RSA_get0_e(key_pair), nullptr);
    EXPECT_NE(RSA_get0_d(key_pair), nullptr);

    // verify private key factors
    EXPECT_NE(RSA_get0_p(key_pair), nullptr);
    EXPECT_NE(RSA_get0_q(key_pair), nullptr);
}

TEST(CryptoTests, TestPubkeyExtraction) {
    std::string out;
    RSA* key_pair = Crypto::generate_new_key_pair(2048);

    Crypto::get_public_key(key_pair, out);

    EXPECT_FALSE(out.empty());

    // GTEST_COUT << out << std::endl;
}

TEST(CryptoTests, TestPrivkeyExtraction) {
    std::string out;
    RSA* key_pair = Crypto::generate_new_key_pair(2048);

    Crypto::get_private_key(key_pair, out);

    EXPECT_FALSE(out.empty());

    // GTEST_COUT << out << std::endl;
}

int main(int argc, char **argv)  {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

