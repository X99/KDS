FROM ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris
RUN apt update -qq && \
    apt install git cmake build-essential openssl libssl-dev -y

WORKDIR /app
# build/install fmt
RUN git clone --depth 1 --branch 8.0.1 https://github.com/fmtlib/fmt.git && \
    cd fmt && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install
# build/install json
RUN git clone --depth 1 --branch v3.10.2 https://github.com/nlohmann/json.git && \
    cd json && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install
RUN git clone --depth 1 --branch v0.9.4 https://github.com/yhirose/cpp-httplib.git && \
    cd cpp-httplib && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install
WORKDIR /app
COPY . .

RUN ./make_certs.sh
RUN cd build && \
    cmake .. && \
    make

EXPOSE 8080/tcp
WORKDIR /app/build
CMD ./KDS